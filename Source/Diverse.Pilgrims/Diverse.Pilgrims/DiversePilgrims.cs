﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using RimWorld.QuestGen;
using Verse;

namespace Diverse.Pilgrims
{
  [HarmonyPatch(typeof(QuestNode_Root_ReliquaryPilgrims), "GetFactionAndPawnKind")]
  public class Patch_QuestNode_Root_ReliquaryPilgrims
  {
    [HarmonyPostfix]
    private static void InjectRandomPilgrimKind(ref PawnKindDef pawnKind)
    {
      try
      {//pawnKind = PawnKindDefOf.PovertyPilgrim;
        if (pawnKind == PawnKindDefOf.PovertyPilgrim)
        {
          pawnKind = KindSelector(PawnKindDefOf.PovertyPilgrim, "Pilgrim_");
        }
        else if (pawnKind == PawnKindDefOf.WellEquippedTraveler)
        {//pawnKind = PawnKindDefOf.WellEquippedTraveler;
          pawnKind = KindSelector(PawnKindDefOf.WellEquippedTraveler, "Traveler_");
        }
      }
      catch (Exception e)
      {
        Log.Warning("Diverse.Pilgrims: Something went wrong " + e);
        return;
      }
    }
    private static PawnKindDef KindSelector (PawnKindDef originalKind, string defPrefix)
		{
      List<PawnKindDef> pilgrimKindDefs = DefDatabase<PawnKindDef>.AllDefsListForReading//Take all pawnKind Defs
                   .Where(def => def.defName.StartsWith(defPrefix))//Use pawnKinds where it starts with the PreFix 
                   .ToList();//pawnKind added to List

      pilgrimKindDefs.Add(originalKind);
      PawnKindDef pawnKind = pilgrimKindDefs.RandomElementWithFallback(originalKind);
      return pawnKind;
    }
  }
}