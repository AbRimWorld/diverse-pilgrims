﻿using HarmonyLib;
using System.Reflection;
using Verse;

namespace Diverse.Pilgrims
{
  [StaticConstructorOnStartup]
  public static class Startup
  {
    static Startup()
    {
      Harmony harmony = new Harmony("DiversePilgrims");
      harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
  }
}
